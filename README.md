# URL Shortener
This is a simple URL shortener, made at the 6th IT Sprint's hackathon by the **mamma-mia** team.

## Set up
1. Install Docker
1. Clone this repository
1. Run:
    ```bash
    ./manage build
    ./manage up -d
    ```
1. Copy `backend/.env.example` to `backend/.env` and append the  contents of `.env` to `backend/.env`
1. Attach shell to backend container and run
    ```bash
    composer install
    php artisan migrate:refresh
    php artisan serve --host 0.0.0.0
    ```
1. Attach shell to frontend container and run
    ```bash
    yarn install
    yarn start
    ```

## Usage

### Frontend
Open URL that is displayed after the `yarn start` was ran.

### API
Open `URLs.postman_collection.json` in Postman and check the examples.
