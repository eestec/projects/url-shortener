<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Url;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class UrlController extends Controller
{
    public function index(): Response
    {
        $url = Url::all();
        return response($url, 200);
    }

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'short_url' => 'required|regex:/^[0-9a-zA-Z_\-.]+$/',
            'target_url' => 'required|url',
            'expiration_date' => 'required',
        ]);


        if (Url::where('short_url', '=', $request['short_url'])->whereDate('expiration_date', '>', date("Y-m-d"))->exists()) {
            $counter = 0;
            $shortUrl = $request['short_url'] . $counter;
            while (Url::where('short_url', '=', $shortUrl)->exists()) {
                $counter = $counter + 1;
                $shortUrl = $request['short_url'] . $counter;
            }
            return response()->json($shortUrl, 409);
        }

        $url = new Url();
        $url->target_url = $request['target_url'];
        $url->short_url = $request['short_url'];
        $url->expiration_date = $request['expiration_date'];
        $url->save();
        return response()->json($url, 201);
    }

    public function show(Url $url): Response
    {
        return response($url, 200);
    }

    // public function update(Request $request, Url $url) : Response {

    //     $url->content = $request['title'];      
    //     $url->save();

    //     return response($url, 200);
    // }

    public function destroy(Url $url): Response
    {
        $url->delete();
        return response($url, 204);
    }

    public function redirect($url)
    {
        $response = Url::firstWhere('short_url', $url);

        return redirect()->away($response['target_url']);
    }
}
