import React from "react";
// import Button from "../../components/Button/Button";
import { useState } from "react";
import QRCode from "react-qr-code";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";

import { Box } from "@mui/system";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

const today = new Date();
let tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);
tomorrow = tomorrow.toISOString().split("T")[0];

const Homepage = () => {
  const [data, setData] = useState({
    target_url: "",
    short_url: "",
    previous_url: "",
    submit_disabled: true,
    expiration_date: tomorrow,
    errors: "",
    qr_code_visible: false,
  });
  const handleDateChange = (e) => {
    console.log(e);
  };

  const handleSubmit = async () => {
    try {
      const res = await fetch("http://localhost:8000/urls", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      console.log(res.status);

      if (res.status === 409) {
        const res_data = await res.json();
        setData((prevState) => ({
          ...prevState,
          errors:
            "This link is a duplicate, please modify, suggested url: " +
            res_data,
        }));
      } else if (res.status === 201) {
        const res_data = await res.json();
        console.log(res_data);
        setData({
          target_url: "",
          short_url: "",
          previous_url: "http://localhost:8000/" + data.short_url,
          submit_disabled: true,
          expiration_date: tomorrow,
          errors: "",
          qr_code_visible: true,
        });
      } else {
        setData((prevState) => ({
          ...prevState,
          errors: "Unknown error " + res.status + " " + res.body,
        }));
      }
      console.log("Hello wolrd");
    } catch (e) {
      console.log("test 5 crazy");
      setData((prevState) => ({
        ...prevState,
        errors: e.toString(),
      }));
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    console.log(name);
    setData((prevState) => {
      let newState = {
        ...prevState,
        [name]: value,
      };

      // TODO validation

      const isValidUrl = (urlString) => {
        try {
          return Boolean(new URL(urlString));
        } catch (e) {
          return false;
        }
      };

      const isValidShortUrl = (urlString) => {
        var urlPattern = new RegExp("^[0-9a-zA-Z_\\-.]+$");
        return !!urlPattern.test(urlString);
      };

      console.log(
        "Is not empty" + !!(newState.target_url && newState.short_url)
      );
      console.log("Is valid url" + isValidUrl(newState.target_url));
      console.log("is valid short url" + isValidShortUrl(newState.short_url));

      let is_input_valid = false;
      if (!newState.target_url) {
        newState.errors = "Empty input boxes";
      } else if (!newState.short_url) {
        newState.errors = "Empty input boxes";
      } else if (!isValidUrl(newState.target_url)) {
        newState.errors = "Target url not valid";
      } else if (!isValidShortUrl(newState.short_url)) {
        newState.errors = "Short url not valid";
      } else {
        newState.errors = "";
        is_input_valid = true;
      }
      console.log("Input valid" + is_input_valid);

      newState = {
        ...newState,
        submit_disabled: !is_input_valid,
      };
      return newState;
    });
  };

  return (
    // <div>
    //   <form>
    //     <label>Target URL:</label>
    //     <input
    //       type="text"
    //       id="target_url"
    //       name="target_url"
    //       placeholder="Website URL"
    //       onChange={handleChange}
    //       value={data.target_url}
    //     />

    //     <label>Short URL</label>
    //     <input
    //       type="text"
    //       id="short_url"
    //       name="short_url"
    //       placeholder="Short URL"
    //       onChange={handleChange}
    //       value={data.short_url}
    //     />

    //     <output name="result" htmlFor="short_url"></output>

    //     <label> Expiration </label>
    //     <input
    //       type="date"
    //       id="expiration_date"
    //       name="expiration_date"
    //       value={data.expiration_date}
    //       min={tomorrow}
    //       onChange={handleChange}
    //     />

    //     <Button onClick={handleSubmit} disabled={data.submit_disabled}>
    //       Shorten the URL
    //     </Button>
    //   </form>
    //   <p>{data.errors}</p>
    //   {data.qr_code_visible && (
    //     <a href={data.previous_url}>{data.previous_url}</a>
    //   )}
    //   <p> Your QR Code is: </p>
    //   <div
    //     style={{
    //       height: "auto",
    //       margin: "0 auto",
    //       max_width: 64,
    //       width: "100%",
    //     }}
    //   >
    //     {data.qr_code_visible && (
    //       <QRCode
    //         size={256}
    //         style={{
    //           height: "auto",
    //           maxWidth: "50%",
    //           width: "50%",
    //         }}
    //         value={data.previous_url}
    //         viewBox={`0 0 256 256`}
    //       />
    //     )}
    //   </div>
    // </div>
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box>
        <Paper
          sx={{ height: "auto", display: "flex", flexDirection: "column" }}
          elevation={12}
        >
          <h1 style={{ textAlign: "center" }}>URL Shortener</h1>
          <TextField
            onChange={handleChange}
            value={data.short_url}
            sx={{ margin: "2rem", marginBottom: "0" }}
            required
            name="short_url"
            label="Short URL"
            type="text"
            id="short_url"
          />
          <TextField
            onChange={handleChange}
            sx={{ margin: "2rem" }}
            value={data.target_url}
            required
            name="target_url"
            label="Target URL"
            type="text"
            id="target_url"
          />
          <DatePicker
            disablePast
            name="name"
            label="Expiration Date"
            value={data.expiration_date}
            onChange={(newValue) => {
              setData((prevState) => {
                let newState = {
                  ...prevState,
                  expiration_date: newValue,
                };
                return newState;
              });
            }}
            renderInput={(params) => <TextField {...params} />}
          />
          <Button
            onClick={handleSubmit}
            sx={{ margin: "1rem" }}
            variant="contained"
            disabled={data.submit_disabled}
          >
            Submit
          </Button>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <p style={{ color: "red", fontSize: "20px" }}>{data.errors}</p>
            {data.qr_code_visible && (
              <a href={data.previous_url} target="blank">{data.previous_url}</a>
            )}
            <div>
              {data.qr_code_visible && (
                <div>
                  <p> Your QR Code is: </p>
                  <QRCode
                    size={256}
                    style={{
                      height: "auto",
                      maxWidth: "100%",
                      width: "100%",
                    }}
                    value={data.previous_url}
                    viewBox={`0 0 256 256`}
                  />
                </div>
              )}
            </div>
          </div>
        </Paper>
      </Box>
    </LocalizationProvider>
  );
};

export default Homepage;
