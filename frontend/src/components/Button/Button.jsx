import React from "react";

function Button({ onClick, children, disabled }) {
  const onClickHandler = async (e) => {
    e.preventDefault();
    onClick();
  };
  return (
    <button type="submit" onClick={onClickHandler} disabled={disabled}>
      {children}
    </button>
  );
}

export default Button;
